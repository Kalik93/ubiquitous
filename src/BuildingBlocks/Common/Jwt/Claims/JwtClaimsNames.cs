namespace U.Common.Jwt.Claims
{
    public static class JwtClaimsTypes
    {
        public static string Sub = "sub";
        public static string UniqueName = "unique_name";
        public static string Name = "name";
        public static string Nickname = "nickname";
        public static string Iat = "iat";
        public static string Email = "email";
        public static string Gender = "gender";
        public static string Address = "address";
        public static string Audience = "aud";
        public static string Issuer = "iss";
        public static string NotBefore = "nbf";
        public static string Expiration = "exp";
        public static string UpdatedAt = "updated_at";
        public static string IssuedAt = "iat";
        public static string Nonce = "nonce";
        public static string Jti = "jti";
        public static string Events = "events";
        public static string ClientId = "client_id";
        public static string Scope = "scope";
        public static string Id = "id";
        public static string IdentityProvider = "idp";
        public static string Role = "role";
        public static string ReferenceTokenId = "reference_token_id";
        public static string Confirmation = "cnf";
        public static string AccessToken = "access_token";

    }
}
