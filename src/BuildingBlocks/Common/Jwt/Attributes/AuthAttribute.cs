using Microsoft.AspNetCore.Authorization;

namespace U.Common.Jwt.Attributes
{
    public class AuthAttribute : AuthorizeAttribute
    {
        public AuthAttribute(string scheme, string policy = "") : base(policy)
        {
            AuthenticationSchemes = scheme;
        }
    }
}