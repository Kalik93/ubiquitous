namespace U.Common.Mvc
{
    public interface ISelfInfoService
    {
         string Id { get; }
         string Name { get; }
    }
}