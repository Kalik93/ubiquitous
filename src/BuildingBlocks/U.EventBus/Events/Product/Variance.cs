namespace U.EventBus.Events.Product
{
    public class Variance
    {
        public string Prop { get; set; }
        public object ValueA { get; set; }
        public object ValueB { get; set; }
    }
}