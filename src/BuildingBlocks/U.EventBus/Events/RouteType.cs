namespace U.EventBus.Events
{
    public enum RouteType
    {
        Primary,
        SignalRProxied,
        EmailProxied
    }
}