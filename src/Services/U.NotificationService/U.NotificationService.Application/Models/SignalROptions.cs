namespace U.NotificationService.Application.Models
{
    public class SignalROptions
    {
        public string RedisConnectionString { get; set; }
    }
}