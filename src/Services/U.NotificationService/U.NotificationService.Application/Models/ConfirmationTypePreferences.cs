namespace U.NotificationService.Application.Models
{
    public class ConfirmationTypePreferences
    {
        public bool SeeUnread { get; set; }
        public bool SeeRead { get; set; }
    }
}