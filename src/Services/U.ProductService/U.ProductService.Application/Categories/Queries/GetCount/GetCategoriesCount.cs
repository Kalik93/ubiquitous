using MediatR;

namespace U.ProductService.Application.Categories.Queries.GetCount
{
    public class GetCategoriesCount : IRequest<int>
    {

    }
}