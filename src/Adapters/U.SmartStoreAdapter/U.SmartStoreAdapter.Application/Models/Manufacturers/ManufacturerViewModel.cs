namespace U.SmartStoreAdapter.Application.Models.Manufacturers
{
    public class ManufacturerViewModel : ManufacturerDto
    {
        public int Id { get; set; }
    }
}