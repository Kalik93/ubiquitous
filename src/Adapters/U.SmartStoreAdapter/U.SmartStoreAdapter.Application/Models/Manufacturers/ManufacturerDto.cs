namespace U.SmartStoreAdapter.Application.Models.Manufacturers
{
    public class ManufacturerDto
    {
        public string Name { get; set; }  
        public string Description { get; set; }
        public int? PictureId { get; set; }
    }
}