namespace U.SmartStoreAdapter.Application.Models.QueryModels
{
    public class StockQuantity
    {
        public int QuantityFrom { get; set; }
        public int QuantityTo { get; set; }
    }
}