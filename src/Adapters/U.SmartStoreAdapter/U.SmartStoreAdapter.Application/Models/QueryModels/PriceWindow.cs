namespace U.SmartStoreAdapter.Application.Models.QueryModels
{
    public class PriceWindow
    {
        public decimal PriceFrom { get; set; }
        public decimal PriceTo { get; set; }
    }
}