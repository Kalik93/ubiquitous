namespace U.SmartStoreAdapter.Application.Models.QueryModels
{
    public enum CountryEnum
    {
        Unspecified,
        Poland,
        Germany,
        Ukraine,
        OutsideTheWorld
    }
}