﻿namespace U.SmartStoreAdapter.Domain.Entities.Media
{
	public class MediaStorage : BaseEntity
	{
		/// <summary>
		/// Binary notificationNavBarToggled
		/// </summary>
		public byte[] Data { get; set; }
	}
}
