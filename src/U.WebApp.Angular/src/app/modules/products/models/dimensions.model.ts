class Dimensions {
  length: number;
  width: number;
  height: number;
  weight: number;
}
