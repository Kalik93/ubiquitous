export interface Category {
  categoryId: string;
  name: string;
  description: string;
  categoryParentId?: number;
}
