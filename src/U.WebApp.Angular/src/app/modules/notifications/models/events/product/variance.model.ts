export interface Variance
{
  prop: string;
  ValueA: object;
  ValueB: object;
}
