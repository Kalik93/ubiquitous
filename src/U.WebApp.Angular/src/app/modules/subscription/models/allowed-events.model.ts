import {IntegrationEventType} from "../../notifications/models/integration-event-type.model";

export interface AllowedEvents
{
  events: IntegrationEventType[];
}
